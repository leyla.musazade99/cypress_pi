// function getBearerToken() {
//   return cy.request({
//     method: 'POST',
//     url: 'https://simple-books-api.glitch.me//api-clients/', // Replace 'your-token-endpoint' with the actual token endpoint URL
//     body: {
      
//       clientName:"Leyla",
//    clientEmail: "l@gmail.com",
//     }
//   }).then((response) => {
//     expect(response.status).to.eq(201);
//       const token = response.body.token;
//     // Extract and return the bearer token from the JSON response
//     return response.body.token;
//   });
// }

// describe('template spec', () => {
//   it('passes', () => {
//     cy.request('GET','https://simple-books-api.glitch.me/status').then(response=>{
//       expect(response.status).to.eq(200);
//   })
// })
// it('passes', () => {
//   cy.request('GET','https://simple-books-api.glitch.me/books').then(response=>{
//     expect(response.status).to.eq(200);
// })
// })
// it('passes', () => {
//   cy.request('GET','https://simple-books-api.glitch.me/books/1').then(response=>{
//     expect(response.status).to.eq(200);

// })
// })

// getBearerToken().then((token) => {
//   cy.request({
//     method: 'POST',
//     url: 'https://simple-books-api.glitch.me/orders',
//     headers: {
//       'Content-Type': 'application/json',
//       'Authorization': `Bearer ${token}` // Include the bearer token in the Authorization header
//     },
//     body: {
//       "bookId": 101,
//       "customerName": "Leyla"
//     }
// })
// })
// })
// Utility function to generate a random email address
//  function generateRandomEmail(){
//   const timestamp = new Date().getTime(); 
//   const username = `user${Math.floor(Math.random() * 1000000)}`;
//   const domain = 'gmail.com'; 
//   return `${username}${timestamp}@${domain}`;;
// }

let token;


describe('template spec', () => {
  it('get bearer token', () => {
cy.generateRandomEmail().then(randomEmail=>{
  cy.request('POST', 'https://simple-books-api.glitch.me/api-clients/',
    {
     clientName: "Leyla",
     clientEmail: randomEmail
   }).then(response => {
   expect(response.status).to.eq(201);
   token = response.body.accessToken;
 });
})
    
  });

  it('returns status 200 for /status endpoint', () => {
    cy.request('GET', 'https://simple-books-api.glitch.me/status').then(response => {
      expect(response.status).to.eq(200);
    });
  });

  it('returns status 200 for /books endpoint', () => {
    cy.request('GET', 'https://simple-books-api.glitch.me/books').then(response => {
      expect(response.status).to.eq(200);
    });
  });

  it('returns status 200 for /books/1 endpoint', () => {
    cy.request('GET', 'https://simple-books-api.glitch.me/books/1').then(response => {
      expect(response.status).to.eq(200);
    });
  });

  it('post orders', () => {
    cy.request({
      method: 'POST',
      url: 'https://simple-books-api.glitch.me/orders',
      headers: {
        authorization: `Bearer ${token}`
      },
      body: {
        bookId: 1,
        customerName: "Leylaopj"
      }
    }).then(response => {
      expect(response.status).to.eq(201);
    });
  });

  after(()=>{
    cy.log(token);
  })
});


